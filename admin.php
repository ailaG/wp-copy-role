<?php
	if (!current_user_can('manage_options')) {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}


	if (isset($_POST['submitted_flag']) && $_POST['submitted_flag']=='True') {
		// Check if input is OK
		$required = Array('role_to_use', 'user');
		$is_form_ok = true;
		foreach ($required as $field)
			if (!isset($_POST[$field]) || trim($_POST[$field]) == '')
				$is_form_ok = false;
		if (!$is_form_ok)
			echo <<<error
				<div class="notice notice-error">
					Error: Please fill all fields.
				</div>
error;
		else {
			$success = False;
			$user = get_user_by('slug', trim($_POST['user']));
			if ($user === False) {
				$success = False;
				echo <<<error
					<div class="notice notice-error">
						Error: not an existing user.
					</div>
error;
			} else {
				$success = BTUserRoles::networkAssignUser($user->ID, $_POST['role_to_use'], $_POST['sites']);
			}
			if ($success === true)
				echo <<<success
					<div class="notice notice-success">
						Update finished.
					</div>
success;
			else
				echo <<<error
					<div class="notice notice-error">
						An error occurred. See log above or contact devs.
					</div>
error;
		}
	}

?>


<div class="wrap">
	<?php screen_icon(); ?>

	<style type="text/css">
		fieldset {
			margin: 0.5em;
		}
		.roles, .sites { 
			display: inline-block; 
			vertical-align: top; 
		}
		.roles label,
		.sites label { 
			display: block; 
		}
	</style>

	<form method="post" action="">
		<input type="hidden" name="submitted_flag" value="True"><?php # If this exists, we know that the user has posted something ?>
		<fieldset class="roles">
			<?php	
				$roles = BTUserRoles::getRoles();
			?>
			<h2>Role to assign:	</h2>
			<?php foreach ($roles as $slug=>$role): ?>
				<label>
					<input type="radio" name="role_to_use" value="<?php echo $slug; ?>">
					<?php echo $role['name']; ?>
				</label>
			<?php endforeach; ?>
		</fieldset>

		<fieldset class="sites">
			<?php
				$sites = wp_get_sites();
			?>
			<h2>Sites:</h2>
			<?php foreach ($sites as $site): ?>
				<label>
					<input 
						type="checkbox" 
						name="sites[]"
						value="<?php echo $site['blog_id']; ?>"
						checked >
					<?php echo $site['domain']; ?>
				</label>
			<?php endforeach; ?>

		</fieldset>

		<fieldset class="users">
			<?php
				//$users = get users();
			?>
			<h2>User:</h2>
			<input type="text" name="user" placeholder="user_name">
		</fieldset>

		<?php submit_button(); ?>
	</form>
</div>