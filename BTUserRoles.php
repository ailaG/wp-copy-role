<?php
/*
Plugin Name: BT User Roles
Description: User role duplicator / manager for Boredom Therapy
Author: Galia Bahat <Galia@Galiaba.com>
Version: 0.9.3
*/

// TODO: limit permissions, make admin suggest usernames


class BTUserRoles {
	public static $verbose = True;
	public static function getRoles() {
		global $wp_roles;
		return $wp_roles->roles;
	}

	public static function networkAssignUser($user_id, $role='subscriber', $site_ids) {
		// Assign user $user_id to $sites with role $role
		// $site_ids is an array of site IDs. If empty, apply to all sites.
		$user_id = (int) $user_id;
		if ($user_id === 0) return;
		if (!is_array($site_ids)) {
			$sites = wp_get_sites();
			$site_ids = array_map(function($site) {
					return $site['blog_id'];
				}, $sites);
		}
		$res = true;
		array_map(function ($site_id) use ($user_id, $role) {
			//$site_id = $site['blog_id'];
			$site_id = (int) $site_id;
			$site_details = get_blog_details($site_id);
			if ($site_details === False) {
				if (self::$verbose == True) {
					print "Bad site ID $site_id";
					return;
				}
			}
			$site_domain = $site_details->domain;
			if (is_user_member_of_blog( $user_id, $site_id ) == True) {
				if (self::$verbose == True)
					print "Cannot add user $user_id to site $site_id/$site_domain: user already a member.<br>";
				return false;
			}
			if (self::$verbose == True)
				print "Adding user $user_id as '$role' to site $site_id/$site_domain.<br>";
			$added = add_user_to_blog($site_id, $user_id, $role);
			if ($added !== True)
				$res = False;
			return $added;
		}, $site_ids);
		return $res;
	}

}


/*** ADMIN ***/

require_once(ABSPATH.'wp-includes/pluggable.php'); // otherwise add_submenu_page etc get buggy

add_action('admin_menu', 'BTUserRoles_add_admin_page');
add_action('network_admin_menu', 'BTUserRoles_add_admin_page');
function BTUserRoles_add_admin_page (){
	add_submenu_page( 'users.php', 'BTUserRoles Settings', 'BTUserRoles', 'manage_options', 'BTUserRoles', 
		function() { require_once(plugin_dir_path( __FILE__ ) . '/admin.php'); } 
	);
};
